# Code Test Application for Yan3k that let's user search for repositories in GitHub by language or topic using GitHub’s well documented APIs. #





##  How to install dependencies and run the application ##

* Just click on this https://drive.google.com/file/d/18r13iZ-t0Cz5Ju1696ew_tyYrNXZl4LQ/view?usp=sharing link in mobile it will ask for install.





## Updates in Version 1.0 ##

1. Write a small program/app that let's user search for repositories in GitHub by language or topic using GitHub’s well documented APIs. You can use either v3 or v4 API. Your search results should be paginated. - DONE. 

2. Generate a report for admin based on the searches done in your application that also includes the results of the searches. - DONE.





## Updates in Version 1.1 ##

1. Make api calls by passing a get parameter bag rather than appending the parameters to the api url itself. - DONE.

2. Remove unnecessary commented codes and empty lines. - DONE.

3. Try using Retrofit for REST calls ( if can ). - DONE.

4. For the admin report, list the information rather than providing PDF links. Still can add a link to download the PDF. - DONE.

5. Make sure the pdf is downloaded properly on to the phone, currently cant view the downloaded file  (not mandatory). - DONE.

6. Try modularising code for better unit testing . - DONE.

7. Add README.md file to the repository rather than explaining in the email. - DONE.





## Setup the project in Android studio and run tests ##

* Download the project code, preferably using `git clone`. (git clone https://Venkateswarlu_SKP@bitbucket.org/Venkateswarlu_SKP/yan3kgithubreposearchapp.git)
* In Android Studio, select *File* | *Open...* and point to the `./build.gradle` file.
* Make sure you select "Unit Tests" as the test artifact in the "Build Variants" panel in Android Studio. 
* Check out the relevant code:
    * The application code is located in `src/main/java`
    * Unit Tests are in `src/test/java`
* Create a test configuration with the JUnit4 runner: `org.junit.runners.JUnit4`
    * Open *Run* menu | *Edit Configurations*
    * Add a new *JUnit* configuration
    * Choose module *app*
    * Select the class to run by using the *...* button
* Run the newly created configuration

The unit test will be ran automatically.





## Instructions for running UI Test cases ##

*  open following path in Android Studio \AndroidStudioProjects\Yan3kGitHubRepoSearchApp\app\src\androidTest\java\com\skp\yan3kgithubreposearchapp\AllTestsSuit.kt then right click on file and select option "Run AllTestSuit".





## Instructions for running Unit Test cases ##

*  open following path in Android Studio \AndroidStudioProjects\Yan3kGitHubRepoSearchApp\app\src\test\java\com\skp\yan3kgithubreposearchapp\GitHubRepoSearchPresenterTest.kt then right click on file and select option "Run GitHubRepoSearchPresenterTest".
