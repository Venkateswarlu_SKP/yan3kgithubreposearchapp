package com.skp.yan3kgithubreposearchapp

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.skp.yan3kgithubreposearchapp.activities.GitHubRepoSearchActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ReposRecyclerViewTest
{
    @get:Rule
    var mGitHubRepoSearchActivityTestRule = ActivityTestRule(GitHubRepoSearchActivity::class.java)

    @Test
    fun testNewEpisodesRecyclerView()
    {
        onView(ViewMatchers.withId(R.id.rv_github_repos)).perform(ViewActions.click())

        onView(ViewMatchers.withId(R.id.rv_github_repos)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                3,
                ViewActions.click()
            )
        )

        onView(ViewMatchers.withId(R.id.rv_github_repos))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(2))
    }








}