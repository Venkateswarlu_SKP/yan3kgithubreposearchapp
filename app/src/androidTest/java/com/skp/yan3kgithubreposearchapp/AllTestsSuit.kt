package com.skp.yan3kgithubreposearchapp

import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses

@RunWith(Suite::class)
@SuiteClasses(ExampleInstrumentedTest::class, GitHubRepoSearchActivityTest::class, ReposRecyclerViewTest::class)
class AllTestsSuit