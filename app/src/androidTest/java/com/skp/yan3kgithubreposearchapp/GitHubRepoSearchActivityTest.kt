package com.skp.yan3kgithubreposearchapp

import android.content.Context
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.skp.yan3kgithubreposearchapp.activities.GitHubRepoSearchActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.core.Is
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class GitHubRepoSearchActivityTest
{
    @get:Rule
    var mGitHubRepoSearchActivityTestRule = ActivityTestRule(GitHubRepoSearchActivity::class.java)


    @Test
    fun testToolbarTitle() {
        val title = ApplicationProvider.getApplicationContext<Context>().getString(R.string.app_name)
        matchToolbarTitle(title)
    }


    private fun matchToolbarTitle(title: CharSequence): ViewInteraction {
        return onView(isAssignableFrom(Toolbar::class.java))
            .check(matches(withToolbarTitle(Is.`is`(title))))
    }

    private fun withToolbarTitle(textMatcher: Matcher<CharSequence>): Matcher<Any> {
        return object : BoundedMatcher<Any, Toolbar>(Toolbar::class.java) {
            public override fun matchesSafely(toolbar: Toolbar): Boolean {
                return textMatcher.matches(toolbar.title)
            }

            override fun describeTo(description: Description) {
                description.appendText("with toolbar title: ")
                textMatcher.describeTo(description)
            }
        }
    }


    @Test
    fun testReposRecyclerView()
    {
        onView(withId(R.id.rv_github_repos))
            .perform(ViewActions.click())

        onView(ViewMatchers.withId(R.id.rv_github_repos))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, ViewActions.click()))

        onView(ViewMatchers.withId(R.id.rv_github_repos))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(2))

    }




}