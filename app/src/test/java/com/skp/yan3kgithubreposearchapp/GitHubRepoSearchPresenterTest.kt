package com.skp.yan3kgithubreposearchapp

import com.skp.yan3kgithubreposearchapp.presenters.GitHubRepoSearchPresenter
import com.skp.yan3kgithubreposearchapp.views.GitHubRepoSearchView
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GitHubRepoSearchPresenterTest {
    @Mock
    private val view: GitHubRepoSearchView? = null

    @Mock
    private var presenter: GitHubRepoSearchPresenter? = null

    @Before
    fun setUp() {
        presenter = GitHubRepoSearchPresenter(view!!)
    }

    @Test
    fun shouldShowErrorMessageWhenSearchInputIsEmpty() {
        Mockito.`when`(view!!.getSearchInput()).thenReturn("")
        presenter!!.onSearchClicked()
        Mockito.verify(view)!!.showInvalidSearchInputErrorAlert(
            R.string.title_wrong_format,
            R.string.message_worng_format,
            R.string.got_it,
            R.string.cancel,
            R.string._db7b1d
        )
    }

    @Test
    fun shouldShowErrorMessageWhenSearchInputIsIncorrectFormat() {
        Mockito.`when`(view!!.getSearchInput()).thenReturn("Animation")
        presenter!!.onSearchClicked()
        Mockito.verify(view)!!.showInvalidSearchInputErrorAlert(
            R.string.title_wrong_format,
            R.string.message_worng_format,
            R.string.got_it,
            R.string.cancel,
            R.string._db7b1d
        )
    }

    @Test
    fun shouldStartSearchWhenSearchInputIsCorrectFormat() {
        Mockito.`when`(view!!.getSearchInput())
            .thenReturn("Animation,Kotlin")
        presenter!!.onSearchClicked()
        Mockito.verify(view, Mockito.times(2))
            .updatePullToRefresh(true)
    }
}