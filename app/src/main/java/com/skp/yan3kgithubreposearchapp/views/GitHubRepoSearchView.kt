package com.skp.yan3kgithubreposearchapp.views

import com.skp.yan3kgithubreposearchapp.database.DatabaseClient
import com.skp.yan3kgithubreposearchapp.pojos.ItemsVo


interface GitHubRepoSearchView
{
    fun showInvalidSearchInputErrorAlert(titleResId: Int, messageResId: Int, positiveBtnTextResId: Int, negativeBtnTextResId: Int, themeColorStringResId: Int)
    fun getSearchInput(): String?
    fun clearAllAdapterItems()
    fun updatePullToRefresh(refreshStatus: Boolean)
    fun addLoadingToAdapter()
    fun removeLoadingFromAdapter()
    fun showNoInternetConnectionErrorToast()
    fun showApiResponseErrorToast(message: String?)
    fun addItemsToAdapter(repoItemsList: List<ItemsVo?>)
    fun getAdapterItemCount(): Int
    fun updateRecyclerViewVisibility()
    fun getDatabaseClientInstance(): DatabaseClient
}