package com.skp.yan3kgithubreposearchapp.presenters

import android.text.TextUtils
import com.google.gson.Gson
import com.skp.yan3kgithubreposearchapp.Globals
import com.skp.yan3kgithubreposearchapp.R
import com.skp.yan3kgithubreposearchapp.constants.DateTimeFormats
import com.skp.yan3kgithubreposearchapp.constants.Keys
import com.skp.yan3kgithubreposearchapp.database.ReposSearchResultsEntity
import com.skp.yan3kgithubreposearchapp.listeners.PaginationListener
import com.skp.yan3kgithubreposearchapp.pojos.GetReposApiResponseVo
import com.skp.yan3kgithubreposearchapp.pojos.ItemsVo
import com.skp.yan3kgithubreposearchapp.views.GitHubRepoSearchView
import kotlincodes.com.retrofitwithkotlin.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

class GitHubRepoSearchPresenter
{


    private val gitHubRepoSearchView: GitHubRepoSearchView

    private var searchClickedDateTime: String = ""

    private var selectedTopic: String

    private var selectedLanguage: String

    private var itemCount = 0

    private var currentPage: Int

    private var isLastPage = false

    private var isLoading = false

    private var totalPage = 50


    constructor(gitHubRepoSearchView: GitHubRepoSearchView)
    {
        this.gitHubRepoSearchView = gitHubRepoSearchView
        this.selectedTopic = Keys.DEFAULT_TOPIC
        this.selectedLanguage = Keys.DEFAULT_LANGUAGE
        this.currentPage = PaginationListener.PAGE_START
        resetItems()
    }



    fun onSearchClicked()
    {
        val queryString = gitHubRepoSearchView.getSearchInput()


        val queryList = queryString!!.split(",")

        if (queryList.size != 2)
        {
            gitHubRepoSearchView.showInvalidSearchInputErrorAlert(R.string.title_wrong_format, R.string.message_worng_format, R.string.got_it, R.string.cancel, R.string._db7b1d)
            return
        }

        getReposBasedOnQuery(queryList.get(0).trim(), queryList.get(1).trim())

    }




    private fun getReposBasedOnQuery(selectedTopic: String, selectedLanguage: String)
    {

        this.searchClickedDateTime = Globals.getCurrentDateAndTime(DateTimeFormats.FORMAT_1)

        this.selectedTopic = selectedTopic

        this.selectedLanguage = selectedLanguage

        resetItems()

    }




    internal fun resetItems()
    {
        itemCount = 0;
        currentPage = PaginationListener.PAGE_START;
        isLastPage = false;
        gitHubRepoSearchView.clearAllAdapterItems()
        gitHubRepoSearchView.updatePullToRefresh(true)
        getRepos()
    }





    private fun getRepos()
    {
        ApiClient.getClient.getGitRepositories("$selectedTopic+language:$selectedLanguage", currentPage, PaginationListener.PAGE_SIZE).enqueue(object: Callback<GetReposApiResponseVo>
        {
            override fun onFailure(call: Call<GetReposApiResponseVo>, t: Throwable)
            {

                gitHubRepoSearchView.updatePullToRefresh(false)

                if (currentPage != PaginationListener.PAGE_START) gitHubRepoSearchView.removeLoadingFromAdapter()

                if (t is UnknownHostException)
                {
                    gitHubRepoSearchView.showNoInternetConnectionErrorToast()
                }
                else
                {
                    gitHubRepoSearchView.showApiResponseErrorToast(t?.message)
                }

            }

            override fun onResponse(call: Call<GetReposApiResponseVo>, response: Response<GetReposApiResponseVo>)
            {
                response?.body()?.let { updateUI(response.raw().request().url().toString(), it) }
            }

        })
    }






    private fun updateUI(apiUrl: String, getReposApiResponseVo: GetReposApiResponseVo)
    {

        val repoItemsList = getReposApiResponseVo?.itemsList

        itemCount += repoItemsList!!.size

        if (currentPage != PaginationListener.PAGE_START) gitHubRepoSearchView.removeLoadingFromAdapter()

        gitHubRepoSearchView.addItemsToAdapter(repoItemsList)

        gitHubRepoSearchView.updatePullToRefresh(false)


        if (currentPage < totalPage && repoItemsList!!.size != 0 && gitHubRepoSearchView.getAdapterItemCount() >= PaginationListener.PAGE_SIZE)
        {
            gitHubRepoSearchView.addLoadingToAdapter()
        }
        else
        {
            isLastPage = true
        }

        isLoading = false


        gitHubRepoSearchView.updateRecyclerViewVisibility()


        Globals.log(this, "apiUrl : $apiUrl")

        insertApiInfoToDb(apiUrl, repoItemsList)

    }





    fun loadMoreItems()
    {
        isLoading = true;
        currentPage++;
        getRepos()
    }





    fun getLastPageStatus(): Boolean
    {
        return isLastPage;
    }





    fun getLoadingStatus(): Boolean
    {
        return isLoading;
    }





    private fun insertApiInfoToDb(apiUrl: String, repoItemsList: List<ItemsVo?>?)
    {

        if (TextUtils.isEmpty(searchClickedDateTime))
        {
            return
        }

        val reposSearchResultsEntity = ReposSearchResultsEntity()
        reposSearchResultsEntity.dateTime = this.searchClickedDateTime
        reposSearchResultsEntity.topic = this.selectedTopic
        reposSearchResultsEntity.language = this.selectedLanguage
        reposSearchResultsEntity.apiUrls = apiUrl
        reposSearchResultsEntity.searchResultsResponse = Gson().toJson(repoItemsList)

        Globals.log(this, "reposSearchResultsEntity : $reposSearchResultsEntity")

        gitHubRepoSearchView.getDatabaseClientInstance().appDatabase.reposSearchResultsEntityDao()!!.insert(reposSearchResultsEntity)
    }



}