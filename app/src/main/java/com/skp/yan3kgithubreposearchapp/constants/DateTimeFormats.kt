package com.skp.yan3kgithubreposearchapp.constants

object DateTimeFormats
{


    val FORMAT_1 = "yyyy-MM-dd HH:mm:ss"

    val FORMAT_2 = "MMMM dd, yyyy @ hh:mm:ss a"

    val FORMAT_3 = "@dd-MM-yyyy@HH.mm.ss"



}