package kotlincodes.com.retrofitwithkotlin.retrofit

import com.skp.yan3kgithubreposearchapp.pojos.GetReposApiResponseVo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("search/repositories?sort=stars&order=desc")
    fun getGitRepositories(
        @Query("q") topicAndLanguage: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Call<GetReposApiResponseVo>

}