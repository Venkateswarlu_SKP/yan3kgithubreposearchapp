package com.skp.yan3kgithubreposearchapp.listeners

import android.content.DialogInterface


interface OnNegativeButtonClickListener {
    fun onNegativeButtonClicked(dialog: DialogInterface?)
}