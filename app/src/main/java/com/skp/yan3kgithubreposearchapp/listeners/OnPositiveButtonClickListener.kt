package com.skp.yan3kgithubreposearchapp.listeners

import android.content.DialogInterface


interface OnPositiveButtonClickListener {
    fun onPositiveButtonClicked(dialog: DialogInterface?)
}