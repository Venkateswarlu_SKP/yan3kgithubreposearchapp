package com.skp.yan3kgithubreposearchapp.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.gson.reflect.TypeToken
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.itextpdf.text.*
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.draw.LineSeparator
import com.shashank.sony.fancydialoglib.Animation
import com.shashank.sony.fancydialoglib.FancyAlertDialog
import com.shashank.sony.fancydialoglib.Icon
import com.skp.yan3kgithubreposearchapp.Globals
import com.skp.yan3kgithubreposearchapp.R
import com.skp.yan3kgithubreposearchapp.constants.DateTimeFormats
import com.skp.yan3kgithubreposearchapp.customuicomponents.CustomProgressDialog
import com.skp.yan3kgithubreposearchapp.database.DatabaseClient
import com.skp.yan3kgithubreposearchapp.database.ReposSearchResultsEntity
import com.skp.yan3kgithubreposearchapp.databinding.ActivityReportDetailBinding
import com.skp.yan3kgithubreposearchapp.pojos.ItemsVo
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.util.concurrent.Executors

class ReportDetailActivity : AppCompatActivity()
{

    private lateinit var destFile: File

    private lateinit var binding: ActivityReportDetailBinding

    private lateinit var reposSearchResultsEntity: ReposSearchResultsEntity

    override fun onCreate(savedInstanceState: Bundle?)
    {

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_report_detail)

        setSupportActionBar(binding.toolbar)

        reposSearchResultsEntity =   ((intent.getSerializableExtra(ReposSearchResultsEntity::class.java.canonicalName) as ReposSearchResultsEntity?)!!)

        binding.toolbar.subtitle = binding.toolbar.subtitle.toString() + "  -  " + "Report Detail"

        checkExternalStoragePermission()


    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_report_detail, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {


        when (item.itemId)
        {
            R.id.action_download -> {
                        showDownloadAlert()
                return true
            }

        }


        return super.onOptionsItemSelected(item)
    }



    private fun showDownloadAlert() {

        FancyAlertDialog.Builder(this)
            .setTitle("Confirm!")
            .setBackgroundColor(Color.parseColor("#e00100")) //Don't pass R.color.colorvalue
            .setMessage("Do you want to download this Report ?")
            .setNegativeBtnText("Cancel")
            .setPositiveBtnBackground(Color.parseColor("#e00100")) //Don't pass R.color.colorvalue
            .setPositiveBtnText("Download!")
            .setAnimation(Animation.POP)
            .isCancellable(false)
            .setIcon(R.drawable.ic_download, Icon.Visible)
            .OnPositiveClicked{ showDownloadedSuccessfullyDialog() }
            .OnNegativeClicked {}
            .build()

    }




    private fun showDownloadedSuccessfullyDialog() {


        FancyAlertDialog.Builder(this)
            .setTitle("PDF Downloaded Successfully....!")
            .setBackgroundColor(Color.parseColor("#008000")) //Don't pass R.color.colorvalue
            .setMessage("You Can find the downloaded file in the following location:\n \n $destFile.parent")
            .setNegativeBtnText("No, Thanks!")
            .setPositiveBtnBackground(Color.parseColor("#008000")) //Don't pass R.color.colorvalue
            .setPositiveBtnText("Open Folder")
            .setAnimation(Animation.POP)
            .isCancellable(false)
            .setIcon(R.drawable.ic_baseline_desktop_mac_48, Icon.Visible)
            .OnPositiveClicked{
                val intent = Intent(Intent.ACTION_VIEW)
                val selectedUri = Uri.parse("file://"+destFile.parent)
                intent.setDataAndType(selectedUri, "application/*")
                startActivity(intent)
            }
            .OnNegativeClicked {}
            .build()




    }



    private fun checkExternalStoragePermission()
    {

        TedPermission.with(this).setPermissionListener(object : PermissionListener
        {
            override fun onPermissionGranted() {

                val customProgressDialog = Globals.showCustomProgressDialog(this@ReportDetailActivity)

                Executors.newSingleThreadExecutor().execute { createAndDownloadPDF(customProgressDialog) }

            }
            override fun onPermissionDenied(deniedPermissions: List<String>) {}
        }).setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
            .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .check()



    }


    private fun createAndDownloadPDF(customProgressDialog: CustomProgressDialog)
    {
        val document = Document()


        destFile = File(getExternalFilesDir(null)!!.path + File.separator + getString(R.string.app_name) + File.separator + "Report For Admin "+Globals.convertDateFormat(reposSearchResultsEntity!!.dateTime, DateTimeFormats.FORMAT_1, DateTimeFormats.FORMAT_3) +".pdf")

        Globals.log(this,"destFile : "+destFile)

        destFile.parentFile.mkdirs()

        if (destFile.exists())
        {
            destFile.delete()
        }

        destFile.createNewFile()


        PdfWriter.getInstance(document, FileOutputStream(destFile))

        document.open()

        document.setPageSize(PageSize.A4)
        document.addCreationDate()
        document.addAuthor("Venkateswarlu")
        document.addCreator("Venky")

        val mColorAccent = BaseColor(0, 153, 204, 255)
        val mHeadingFontSize = 20.0f

        val urName = BaseFont.createFont("assets/fonts/lato_semibold.ttf", "UTF-8", BaseFont.EMBEDDED)

        val lineSeparator = LineSeparator()
        lineSeparator.setLineColor(BaseColor(0, 0, 0, 68))


        val titleFont = Font(urName, 36.0f, Font.NORMAL, BaseColor.BLACK)
        val titleChunk = Chunk("Report For Admin", titleFont)
        val titleParagraph = Paragraph(titleChunk)
        titleParagraph.setAlignment(Element.ALIGN_CENTER);
        document.add(titleParagraph);

        val mOrderIdFont = Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent)
        val mOrderIdChunk = Chunk("Search Result for "+Globals.convertDateFormat(reposSearchResultsEntity!!.dateTime,
            DateTimeFormats.FORMAT_1,
            DateTimeFormats.FORMAT_2), mOrderIdFont)
        val mOrderIdParagraph = Paragraph(mOrderIdChunk)
        mOrderIdParagraph.setAlignment(Element.ALIGN_CENTER);
        document.add(mOrderIdParagraph)


        document.add(Paragraph("\n"));
        document.add(Paragraph("\n"));
        document.add(Chunk(lineSeparator));
        document.add(Paragraph("\n"));
        document.add(Paragraph("\n"));


        val allMatchedItemsList = DatabaseClient.getInstance(applicationContext)!!.appDatabase.reposSearchResultsEntityDao()!!.getReposSearchResultsEntityByDateTime(reposSearchResultsEntity.dateTime)


        for (matchedItemsVo in allMatchedItemsList!!)
        {


            val repoItemsList: List<ItemsVo?>? = Globals.getSpecificVoObject(matchedItemsVo.searchResultsResponse, object: TypeToken<List<ItemsVo>>(){}.type)

            val repoItemFont = Font(urName, mHeadingFontSize, Font.NORMAL, BaseColor.BLACK)

            for(itemsVo in repoItemsList!!)
            {

                val image: Image = Image.getInstance(URL(itemsVo!!.ownerVo!!.avatarUrl))
                image.setAlignment(Element.ALIGN_CENTER)
                image.scaleAbsoluteWidth(100f)
                image.scaleAbsoluteHeight(100f)
                document.add(image)
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("Git Repo Name : "+ itemsVo.fullName, repoItemFont))
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("Topic : "+ itemsVo.name, repoItemFont))
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("Language : "+ itemsVo.language, repoItemFont))
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("Forks : "+ itemsVo.forksCount, repoItemFont))
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("Stars : "+ itemsVo.stargazersCount, repoItemFont))
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("Open Issues : "+ itemsVo.openIssuesCount, repoItemFont))
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("About : "+ itemsVo.description, repoItemFont))
                document.add(Paragraph("\n"))
                document.add(getRepoItemParagraph("GitHub Url : "+ itemsVo.htmlUrl, repoItemFont))


                document.newPage()


            }


        }



        document.close()


        runOnUiThread {

            customProgressDialog.dismiss()

            binding.pdfView.fromFile(destFile).load()

        }



    }


    private fun getRepoItemParagraph(content: String, repoItemFont: Font): Element?
    {
        val repoItemChunk = Chunk(content, repoItemFont)
        val repoItemParagraph = Paragraph(repoItemChunk)
        repoItemParagraph.setAlignment(Element.ALIGN_LEFT);
        return repoItemParagraph

    }



    companion object
    {

        fun navigate(activity: Activity?, reposSearchResultsEntity: ReposSearchResultsEntity?)
        {
            activity?.startActivity(Intent(activity, ReportDetailActivity::class.java).putExtra(ReposSearchResultsEntity::class.java.canonicalName,reposSearchResultsEntity))
        }

    }


}