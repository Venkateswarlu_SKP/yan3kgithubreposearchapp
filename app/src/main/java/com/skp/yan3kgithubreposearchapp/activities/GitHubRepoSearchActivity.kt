package com.skp.yan3kgithubreposearchapp.activities

import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ferfalk.simplesearchview.SimpleSearchView
import com.ferfalk.simplesearchview.utils.DimensUtils
import com.google.gson.Gson
import com.shashank.sony.fancydialoglib.Animation
import com.shashank.sony.fancydialoglib.FancyAlertDialog
import com.shashank.sony.fancydialoglib.Icon
import com.skp.yan3kgithubreposearchapp.Globals
import com.skp.yan3kgithubreposearchapp.R
import com.skp.yan3kgithubreposearchapp.adapters.GitHubReposRvAdapter
import com.skp.yan3kgithubreposearchapp.database.DatabaseClient
import com.skp.yan3kgithubreposearchapp.database.ReposSearchResultsEntity
import com.skp.yan3kgithubreposearchapp.databinding.ActivityGitHubRepoSearchBinding
import com.skp.yan3kgithubreposearchapp.listeners.PaginationListener
import com.skp.yan3kgithubreposearchapp.pojos.ItemsVo
import com.skp.yan3kgithubreposearchapp.presenters.GitHubRepoSearchPresenter
import com.skp.yan3kgithubreposearchapp.views.GitHubRepoSearchView


class GitHubRepoSearchActivity : AppCompatActivity(), GitHubRepoSearchView, SimpleSearchView.OnQueryTextListener, GitHubReposRvAdapter.OnItemClickListener
{


    private var queryString: String? = null

    private lateinit var presenter: GitHubRepoSearchPresenter

    private lateinit var binding: ActivityGitHubRepoSearchBinding

    private val EXTRA_REVEAL_CENTER_PADDING = 40



    override fun onCreate(savedInstanceState: Bundle?)
    {

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_git_hub_repo_search)

        setSupportActionBar(binding.toolbar)

        setupRecyclerView()

        presenter = GitHubRepoSearchPresenter(this)

        binding.pullToRefresh.setOnRefreshListener { presenter.resetItems() }

    }




    private fun setupRecyclerView()
    {

        binding.rvGithubRepos.layoutManager = LinearLayoutManager(this)

        binding.rvGithubRepos.adapter = GitHubReposRvAdapter()

        (binding.rvGithubRepos.adapter as GitHubReposRvAdapter).setOnItemClickListener(this)


        binding.rvGithubRepos.addOnScrollListener(object: PaginationListener(binding.rvGithubRepos.layoutManager as LinearLayoutManager)
        {

            override fun loadMoreItems()
            {
                presenter.loadMoreItems()
            }

            override fun isLastPage(): Boolean
            {
                return presenter.getLastPageStatus()
            }

            override fun isLoading(): Boolean
            {
                return presenter.getLoadingStatus()
            }

        })


    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_git_repo_search, menu)
        setupSearchView(menu)
        return true
    }





    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        when (item.itemId) {
            R.id.action_report_for_admin -> {
                ReportForAdminActivity.navigate(this, item.title.toString()); return true
            }
        }

        return super.onOptionsItemSelected(item)
    }





    private fun setupSearchView(menu: Menu?)
    {

        val item = menu!!.findItem(R.id.action_search)

        binding.searchView.setMenuItem(item)

        val revealCenter: Point = binding.searchView.getRevealAnimationCenter()

        revealCenter.x -= DimensUtils.convertDpToPx(EXTRA_REVEAL_CENTER_PADDING, this)

        binding.searchView.setOnQueryTextListener(this)

    }





    override fun showInvalidSearchInputErrorAlert(titleResId: Int, messageResId: Int, positiveBtnTextResId: Int, negativeBtnTextResId: Int, themeColorStringResId: Int)
    {
        FancyAlertDialog.Builder(this)
            .setTitle(getString(titleResId))
            .setMessage(getString(messageResId))
            .setPositiveBtnText(getString(positiveBtnTextResId))
            .setNegativeBtnText(getString(negativeBtnTextResId))
            .setBackgroundColor(Color.parseColor(getString(themeColorStringResId)))
            .setPositiveBtnBackground(Color.parseColor(getString(themeColorStringResId)))
            .setAnimation(Animation.POP)
            .isCancellable(false)
            .setIcon(R.drawable.ic_baseline_pan_tool_24, Icon.Visible)
            .OnPositiveClicked{}
            .OnNegativeClicked {}
            .build()
    }



    override fun getSearchInput(): String?
    {
        return queryString
    }



    override fun clearAllAdapterItems()
    {
        (binding.rvGithubRepos.adapter as GitHubReposRvAdapter).clearAllItems()
    }



    override fun updatePullToRefresh(refreshStatus: Boolean)
    {
        binding.pullToRefresh.setRefreshing(refreshStatus)
    }



    override fun addLoadingToAdapter()
    {
        (binding.rvGithubRepos.adapter as GitHubReposRvAdapter).addLoading()
    }


    override fun removeLoadingFromAdapter()
    {
        (binding.rvGithubRepos.adapter as GitHubReposRvAdapter).removeLoading()
    }


    override fun showNoInternetConnectionErrorToast()
    {
        Globals.toast(this@GitHubRepoSearchActivity,getString(R.string.no_internet_connection_text))
    }



    override fun showApiResponseErrorToast(message: String?)
    {
        Globals.toast(this@GitHubRepoSearchActivity, message)
    }



    override fun addItemsToAdapter(repoItemsList: List<ItemsVo?>)
    {
        (binding.rvGithubRepos.adapter as GitHubReposRvAdapter).addItems(repoItemsList)
    }



    override fun getAdapterItemCount(): Int
    {
        return (binding.rvGithubRepos.adapter as GitHubReposRvAdapter).itemCount
    }



    override fun updateRecyclerViewVisibility()
    {
        binding.rvGithubRepos.visibility = if((binding.rvGithubRepos.adapter as GitHubReposRvAdapter).itemCount > 0) View.VISIBLE else View.GONE
    }




    override fun getDatabaseClientInstance(): DatabaseClient
    {
        return DatabaseClient.getInstance(applicationContext)!!
    }




    override fun onQueryTextSubmit(query: String?): Boolean
    {

        Globals.hideKeyboard(this@GitHubRepoSearchActivity)

        this.queryString = query

        presenter.onSearchClicked()

        return true

    }




    override fun onQueryTextChange(newText: String?): Boolean
    {
        return true
    }




    override fun onQueryTextCleared(): Boolean
    {
        return true
    }




    override fun onItemClick(itemPojo: ItemsVo?)
    {
        GitHubDetailsActivity.navigate(this@GitHubRepoSearchActivity, itemPojo!!)
    }





}