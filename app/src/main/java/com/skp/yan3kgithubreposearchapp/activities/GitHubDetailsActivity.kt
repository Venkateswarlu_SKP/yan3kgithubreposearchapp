package com.skp.yan3kgithubreposearchapp.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import com.skp.yan3kgithubreposearchapp.Globals
import com.skp.yan3kgithubreposearchapp.R
import com.skp.yan3kgithubreposearchapp.databinding.ActivityGitHubDetailsBinding
import com.skp.yan3kgithubreposearchapp.pojos.ItemsVo

class GitHubDetailsActivity : AppCompatActivity()
{

    private lateinit var binding: ActivityGitHubDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_git_hub_details)

        setSupportActionBar(binding.toolbar)

        val itemsVo =   (intent.getSerializableExtra(ItemsVo::class.java.canonicalName) as ItemsVo?)

        binding.toolbar.subtitle = binding.toolbar.subtitle.toString() + "  -  " + itemsVo!!.name

        binding.wvDetailPage.webViewClient = object : WebViewClient()
        {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean
            {
                view?.loadUrl(url)
                return true
            }
        }


        val progressDialog = Globals.showCustomProgressDialog(this)

        progressDialog.show()

        binding.wvDetailPage.webChromeClient = object : WebChromeClient() {

            override fun onProgressChanged(view: WebView?, newProgress: Int) {

                if (newProgress == 100)
                {
                    progressDialog.dismiss()
                }

            }


        }

        binding.wvDetailPage.loadUrl(itemsVo.htmlUrl)

    }

    companion object
    {

        fun navigate(activity: Activity?, itemsVo: ItemsVo)
        {
            activity?.startActivity(Intent(activity, GitHubDetailsActivity::class.java).putExtra(ItemsVo::class.java.canonicalName,itemsVo))
        }

    }


}