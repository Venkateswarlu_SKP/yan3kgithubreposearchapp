package com.skp.yan3kgithubreposearchapp.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.skp.yan3kgithubreposearchapp.R
import com.skp.yan3kgithubreposearchapp.adapters.ReportsForAdminRvAdapter
import com.skp.yan3kgithubreposearchapp.constants.Keys
import com.skp.yan3kgithubreposearchapp.database.DatabaseClient
import com.skp.yan3kgithubreposearchapp.database.ReposSearchResultsEntity
import com.skp.yan3kgithubreposearchapp.databinding.ActivityReportForAdminBinding


class ReportForAdminActivity : AppCompatActivity()
{


    private lateinit var binding: ActivityReportForAdminBinding

    override fun onCreate(savedInstanceState: Bundle?)
    {

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_report_for_admin)

        setSupportActionBar(binding.toolbar)

        binding.toolbar.subtitle = binding.toolbar.subtitle.toString() + "  -  " + intent.getStringExtra(Keys.subTitlePrefix)

        binding.rvReportsForAdmin.layoutManager = GridLayoutManager(this,2)

        binding.rvReportsForAdmin.adapter = ReportsForAdminRvAdapter()


        val itemsList = DatabaseClient.getInstance(applicationContext)!!.appDatabase.reposSearchResultsEntityDao()!!.getDistinctSearchDateTimes()

        (binding.rvReportsForAdmin.adapter as ReportsForAdminRvAdapter).updateItems(itemsList)


        binding.rvReportsForAdmin.visibility = if((binding.rvReportsForAdmin.adapter as ReportsForAdminRvAdapter).itemCount > 0) View.VISIBLE else View.GONE


        (binding.rvReportsForAdmin.adapter as ReportsForAdminRvAdapter).setOnItemClickListener(object: ReportsForAdminRvAdapter.OnItemClickListener{
            override fun onItemClick(itemPojo: ReposSearchResultsEntity?)
            {
                ReportDetailActivity.navigate(this@ReportForAdminActivity,itemPojo)
            }
        })


    }


    companion object
    {

        fun navigate(activity: Activity?, subTitlePrefix: String)
        {
            activity?.startActivity(Intent(activity, ReportForAdminActivity::class.java).putExtra(Keys.subTitlePrefix,subTitlePrefix))
        }

    }



}