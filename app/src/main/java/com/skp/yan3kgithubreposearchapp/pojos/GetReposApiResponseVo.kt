package com.skp.yan3kgithubreposearchapp.pojos

import com.google.gson.annotations.SerializedName

data class GetReposApiResponseVo(

	@field:SerializedName("total_count")
	val totalCount: Int? = null,

	@field:SerializedName("incomplete_results")
	val incompleteResults: Boolean? = null,

	@field:SerializedName("items")
	val itemsList: List<ItemsVo?>? = null
)



