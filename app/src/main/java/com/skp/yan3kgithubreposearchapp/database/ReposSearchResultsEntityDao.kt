package com.skp.yan3kgithubreposearchapp.database

import androidx.room.*

@Dao
interface ReposSearchResultsEntityDao
{
    @get:Query("SELECT * FROM repos_search_results_table")
    val getAll: List<ReposSearchResultsEntity?>?

    @Insert
    fun insert(reposSearchResultsEntity: ReposSearchResultsEntity?)

    @Delete
    fun delete(reposSearchResultsEntity: ReposSearchResultsEntity?)

    @Update
    fun update(reposSearchResultsEntity: ReposSearchResultsEntity?)


    @Query("SELECT * FROM repos_search_results_table WHERE date_time = :dateTime")
    fun getReposSearchResultsEntityByDateTime(dateTime: String): List<ReposSearchResultsEntity>?


    @Query("SELECT * FROM repos_search_results_table group by date_time")
    fun getDistinctSearchDateTimes(): List<ReposSearchResultsEntity>?


}