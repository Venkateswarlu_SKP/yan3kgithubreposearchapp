package com.skp.yan3kgithubreposearchapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "repos_search_results_table")
class ReposSearchResultsEntity : Serializable
{

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "serial_number")
    var serialNumber: Int = 0


    @ColumnInfo(name = "date_time")
    lateinit var dateTime: String

    @ColumnInfo(name = "topic")
    lateinit var topic: String

    @ColumnInfo(name = "language")
    lateinit var language: String

    @ColumnInfo(name = "api_urls")
    lateinit var apiUrls: String

    @ColumnInfo(name = "search_results_response")
    var searchResultsResponse: String? = null



}