package com.skp.yan3kgithubreposearchapp.database

import android.content.Context
import androidx.room.Room
import com.skp.yan3kgithubreposearchapp.Globals
import com.skp.yan3kgithubreposearchapp.R

class DatabaseClient private constructor(mCtx: Context)
{

    val appDatabase: AppDatabase

    companion object {
        private var mInstance: DatabaseClient? = null

        @Synchronized
        fun getInstance(mCtx: Context?): DatabaseClient? {
            if (mInstance == null) {
                mInstance = DatabaseClient(mCtx!!)
            }
            return mInstance
        }
    }

    init
    {
        appDatabase = Room.databaseBuilder(mCtx, AppDatabase::class.java, mCtx.getString(R.string.app_name)+"Db").allowMainThreadQueries().build()
    }


}