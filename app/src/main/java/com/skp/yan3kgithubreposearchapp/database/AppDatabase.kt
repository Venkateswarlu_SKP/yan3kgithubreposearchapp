package com.skp.yan3kgithubreposearchapp.database

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [ReposSearchResultsEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase()
{
    abstract fun reposSearchResultsEntityDao(): ReposSearchResultsEntityDao?
}