package com.skp.yan3kgithubreposearchapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.skp.yan3kgithubreposearchapp.Globals
import com.skp.yan3kgithubreposearchapp.R
import com.skp.yan3kgithubreposearchapp.databinding.RvItemGitReposBinding
import com.skp.yan3kgithubreposearchapp.databinding.RvItemProgressDialogBinding
import com.skp.yan3kgithubreposearchapp.pojos.ItemsVo

class GitHubReposRvAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    private val VIEW_TYPE_LOADING = 0
    private val VIEW_TYPE_NORMAL = 1
    private var isLoaderVisible = false

    private lateinit var recyclerView: RecyclerView

    private var items:ArrayList<ItemsVo?> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
    {
        if (viewType == VIEW_TYPE_NORMAL)
        {
            return MyViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.rv_item_git_repos,parent,false),onItemClickListener)
        }
        else
        {
            return LoadingViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.rv_item_progress_dialog,parent,false),onItemClickListener)
        }


    }


    override fun getItemCount(): Int
    {
        return items.size
    }


    fun clearAllItems()
    {
        items.clear()
        notifyDataSetChanged()
    }




    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int)
    {
        if (getItemViewType(position) == VIEW_TYPE_NORMAL)
        {
            (holder as MyViewHolder).bind(items.get(position))
        }
        else
        {
            (holder as LoadingViewHolder).bind(items.get(position))
        }

    }


    override fun getItemViewType(position: Int): Int
    {
        return if (isLoaderVisible)
        {
            if (position == items.size - 1) VIEW_TYPE_LOADING else VIEW_TYPE_NORMAL
        }
        else
        {
            VIEW_TYPE_NORMAL
        }
    }



    fun updateItems(itemPojosList: List<ItemsVo?>?)
    {

        items.clear()

        if (itemPojosList != null)
        {
            items.addAll(itemPojosList)
        }

        notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation();

    }


    fun addItems(itemPojosList: List<ItemsVo?>?)
    {

        if (itemPojosList != null)
        {
            items.addAll(itemPojosList)
        }

        notifyDataSetChanged()

        if (items.size <= 20)
        {
            recyclerView.scheduleLayoutAnimation()
        }


    }


    fun addLoading()
    {
        isLoaderVisible = true
        items.add(ItemsVo())
        notifyItemInserted(items.size - 1)
    }


    fun removeLoading() {
        isLoaderVisible = false
        val position: Int = items.size - 1
        val item: ItemsVo? = getItem(position)
        if (item != null) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }




    fun getItem(position: Int): ItemsVo? {
        return items.get(position)
    }


    private var onItemClickListener: OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }







    interface OnItemClickListener
    {
        fun onItemClick(itemPojo: ItemsVo??)
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }




    class MyViewHolder : RecyclerView.ViewHolder
    {


        private var onItemClickListener: OnItemClickListener? = null
        val itemBinding: RvItemGitReposBinding

        constructor(itemBinding: RvItemGitReposBinding, onItemClickListener: OnItemClickListener?) : super(itemBinding.root)
        {
            this.itemBinding = itemBinding
            this.onItemClickListener = onItemClickListener
            this.itemBinding.handler = this;
        }

        fun bind(itemPojo: ItemsVo?)
        {
            Globals.log(this,"bind() :: itemPojo : $itemPojo")

            itemBinding.itemPojo = itemPojo

            itemBinding.executePendingBindings()

        }

        fun onItemClick(itemPojo: ItemsVo?)
        {
            onItemClickListener?.onItemClick(itemPojo)
        }




    }


    class LoadingViewHolder : RecyclerView.ViewHolder
    {


        private var onItemClickListener: OnItemClickListener? = null
        val itemBinding: RvItemProgressDialogBinding

        constructor(itemBinding: RvItemProgressDialogBinding, onItemClickListener: OnItemClickListener?) : super(itemBinding.root)
        {
            this.itemBinding = itemBinding
            this.onItemClickListener = onItemClickListener
            this.itemBinding.handler = this;
        }

        fun bind(itemPojo: ItemsVo?)
        {
            Globals.log(this,"bind() :: itemPojo : $itemPojo")

            itemBinding.itemPojo = itemPojo

            itemBinding.executePendingBindings()

        }

        fun onItemClick(itemPojo: ItemsVo?)
        {
            onItemClickListener?.onItemClick(itemPojo)
        }




    }





}

