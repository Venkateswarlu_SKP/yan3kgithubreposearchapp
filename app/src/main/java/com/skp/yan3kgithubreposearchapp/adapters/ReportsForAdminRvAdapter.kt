package com.skp.yan3kgithubreposearchapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.skp.yan3kgithubreposearchapp.Globals
import com.skp.yan3kgithubreposearchapp.R
import com.skp.yan3kgithubreposearchapp.constants.DateTimeFormats
import com.skp.yan3kgithubreposearchapp.database.ReposSearchResultsEntity
import com.skp.yan3kgithubreposearchapp.databinding.RvItemReportsForAdminBinding

class ReportsForAdminRvAdapter : RecyclerView.Adapter<ReportsForAdminRvAdapter.MyViewHolder>()
{


    private lateinit var recyclerView: RecyclerView

    private var items:ArrayList<ReposSearchResultsEntity> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder
    {
        return MyViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.rv_item_reports_for_admin,parent,false),onItemClickListener)
    }





    override fun getItemCount(): Int
    {
        return items.size
    }








    override fun onBindViewHolder(holder: MyViewHolder, position: Int)
    {
        holder.bind(items.get(position))
    }



    fun updateItems(itemPojosList: List<ReposSearchResultsEntity>?)
    {

        items.clear()

        if (itemPojosList != null)
        {
            items.addAll(itemPojosList)
        }

        notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation();

    }





    private var onItemClickListener: OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }







    interface OnItemClickListener
    {
        fun onItemClick(itemPojo: ReposSearchResultsEntity?)
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }




    class MyViewHolder : RecyclerView.ViewHolder
    {


        private var onItemClickListener: OnItemClickListener? = null
        val itemBinding: RvItemReportsForAdminBinding

        constructor(itemBinding: RvItemReportsForAdminBinding, onItemClickListener: OnItemClickListener?) : super(itemBinding.root)
        {
            this.itemBinding = itemBinding
            this.onItemClickListener = onItemClickListener
            this.itemBinding.handler = this;
        }

        fun bind(itemPojo: ReposSearchResultsEntity)
        {
            Globals.log(this,"bind() :: itemPojo : $itemPojo")

            itemBinding.itemPojo = itemPojo

            itemBinding.executePendingBindings()

        }

        fun onItemClick(itemPojo: ReposSearchResultsEntity)
        {
            onItemClickListener?.onItemClick(itemPojo)
        }


        fun getFormattedDateTimeString(itemPojo: ReposSearchResultsEntity): String
        {

            return Globals.convertDateFormat(itemPojo.dateTime, DateTimeFormats.FORMAT_1,DateTimeFormats.FORMAT_2)

        }



    }





}

