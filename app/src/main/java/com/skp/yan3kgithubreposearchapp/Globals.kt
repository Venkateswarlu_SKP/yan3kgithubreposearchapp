package com.skp.yan3kgithubreposearchapp

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.skp.yan3kgithubreposearchapp.Globals.log
import com.skp.yan3kgithubreposearchapp.customuicomponents.CustomProgressDialog
import com.skp.yan3kgithubreposearchapp.listeners.OnNegativeButtonClickListener
import com.skp.yan3kgithubreposearchapp.listeners.OnPositiveButtonClickListener
import java.io.File
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object Globals
{


    fun log(currentObject: Any?, s: String)
    {
        Log.i("SKP", (currentObject?.javaClass?.simpleName ?: "null") + " :: " + s + " @ " + if (currentObject != null) currentObject.javaClass.canonicalName else "null")
    }



    fun toast(context: Context, message: String?)
    {
        Toast.makeText(context,message,Toast.LENGTH_LONG).show()
    }



    fun <T> getSpecificVoObject(
        jsonString: String?,
        typeOfT: Type?
    ): T {
        return Gson().fromJson(jsonString, typeOfT)
    }





    fun showCustomProgressDialog(activity: Activity): CustomProgressDialog {

        val progressDialog = CustomProgressDialog(activity)
        progressDialog.show()
        progressDialog.setContentView(R.layout.progressdialog)
        progressDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        return progressDialog

    }






    fun hideKeyboard(activity: Activity) {
        val view =
            activity.findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }




    fun getCurrentDateAndTime(pattern: String): String
    {
        return SimpleDateFormat(pattern).format(Calendar.getInstance().time)
    }




    fun convertDateFormat(fromDate: String?, fromFormat: String?, toFormat: String?): String
    {
        if (!TextUtils.isEmpty(fromDate))
        {
            try {
                val originalFormat: DateFormat = SimpleDateFormat(fromFormat)
                val targetFormat: DateFormat = SimpleDateFormat(toFormat)
                val date = originalFormat.parse(fromDate)
                val formattedDate = targetFormat.format(date)
                log(null, "formattedDate : $formattedDate")
                return formattedDate
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return ""
    }




}



@BindingAdapter("imageUrl")
fun setImageUrl(imageView: AppCompatImageView, imageUrl: String?)
{

    log(null, "imageUrl : $imageUrl")
    Glide.with(imageView.context).load(imageUrl).placeholder(R.drawable.place_holder).into(imageView);

}








