package com.skp.yan3kgithubreposearchapp.customuicomponents

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.skp.yan3kgithubreposearchapp.R

class SimpleDividerItemDecoration @JvmOverloads constructor(context: Context?, dividerDrawableId: Int, hideFirstDivider: Boolean = false, hideLastDivider: Boolean = false) : ItemDecoration()
{

    private var context: Context
    private val hideFirstDivider: Boolean

    private val hideLastDivider: Boolean

    private val mDivider: Drawable?

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State)
    {

        val left = parent.paddingLeft + context.resources.getDimension(R.dimen._8sdp).toInt()

        val right = parent.width - parent.paddingRight - context.resources.getDimension(R.dimen._8sdp).toInt()

        val childCount = parent.childCount

        for (i in 0 until childCount)
        {

            if (i == 0 && hideFirstDivider || i == childCount - 1 && hideLastDivider)
            {
                continue
            }

            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val top = child.bottom + params.bottomMargin

            val bottom = top + mDivider!!.intrinsicHeight

            mDivider.setBounds(left, top, right, bottom)

            mDivider.draw(c)

        }
    }

    init
    {
        mDivider = ContextCompat.getDrawable(context!!, dividerDrawableId)
        this.hideFirstDivider = hideFirstDivider
        this.hideLastDivider = hideLastDivider
        this.context = context
    }

}